<?php 
$title = 'Template Engine Mixes';
require 'includes/header.php';?>
<main>
  <form enctype="multipart/form-data" action="handler.php" method="POST" class="form-wrap">
  <div class="form__left-input-wrap">
    <label for="mix_name" class="label_simple">Name of mix</label>
    <input type="text" id="mix_name" name="mix_name">


    <label for="time_mix" class="label_simple">Mix duration</label>
    <input type="text" id="time_mix" name="time_mix">

    <label for="mix_tracks" class="label_simple">How many tracks</label>
    <input type="number" id="mix_tracks" name="mix_tracks">


    <label for="date_of_mix" class="label_simple">Date of release</label>
    <input type="text" id="date_of_mix" name="date_of_mix">

    <label class="label_simple">Name of the public</label>

    <div class="radio-wrap">
        <label for="yourtracklist" class="label_radio">
          <input type="radio" name="public" value="yourtracklist" id="yourtracklist" checked>
          yourtracklist
        </label>
        <label for="lost2432615184" class="label_radio">
          <input type="radio" name="public" value="lost2432615184" id="lost2432615184">
          lost2432615184
        </label>
      </div>

  </div>

  <div class="form__image-window-wrap">
    <label class="label_simple">Cover</label>
    <div class="form__image-window" id="image-form"></div>
    <label for="inputfile" class='btn-inputfile'>select a file</label>
    <input name="cover" type="file" id="inputfile" class="inputfile" required
>
  </div>

    <button type="submit" class='btn-submit'>Сreate template</button>

  </form>

  </main>
  <script src="scripts/main.js"></script>
</body>
</html>