<?php

function addNameRel ($imgPath, $text) {

  $img = ImageCreateFromJPEG($imgPath);
  $color = imagecolorallocate($img, 191,213,222);

  $size = 18;
  if (strlen($text) > 24) {
    $size = 16;
  }
  if (strlen($text) > 26) {
    $size = 15;
  }

  if (strlen($text) > 28) {
    $size = 14;
  }

  if (strlen($text) > 30) {
    $size = 13;
  }

  if (strlen($text) > 32) {
    $size = 12;
  }


  $font = __DIR__ . '/fonts/GothamBook.otf';
  imagettftext($img, $size, 0, 21, 39, $color, $font, $text);

  imagejpeg($img, 'addingtext/addingtext1.jpeg', 100);

}


function addNameArtist ($imgPath, $text) {

  $img = ImageCreateFromJPEG($imgPath);
  $color = imagecolorallocate($img, 191,213,222);

  $font = __DIR__ . '/fonts/gothambold.ttf';

  $size = 18;

  if (strlen($text) > 18) {
    $size = 16;
  }
  if (strlen($text) > 20) {
    $size = 15;
  }

  if (strlen($text) > 22) {
    $size = 14;
  }

  if (strlen($text) > 24) {
    $size = 13;
  }

  if (strlen($text) > 26) {
    $size = 12;
  }


  imagettftext($img, $size, 0, 21, 70, $color, $font, 'by ' . $text);

  imagejpeg($img, 'addingtext/addingtext2.jpeg', 100);

}

function addType ($imgPath, $text, $type) {

  $img = ImageCreateFromJPEG($imgPath);
  $color = imagecolorallocate($img, 191,213,222);


  $font = __DIR__ . '/fonts/gothambold.ttf';
  imagettftext($img, 15, 0, 21, 105, $color, $font, $type . ' ' . $text . ' tracks');

  imagejpeg($img, 'addingtext/addingtext3.jpeg', 100);
}

function addDate ($imgPath, $text) {

  $img = ImageCreateFromJPEG($imgPath);
  $color = imagecolorallocate($img, 191,213,222);


  $font = __DIR__ . '/fonts/gothambold.ttf';
  imagettftext($img, 12.5, 0, 21, 178, $color, $font, $text);

  imagejpeg($img, 'final-image/image.jpeg', 100);

}


// Работа с миксами

function addNameMix ($imgPath, $text) {

  $img = ImageCreateFromJPEG($imgPath);
  $color = imagecolorallocate($img, 191,213,222);

  $size = 18;
  if (strlen($text) > 24) {
    $size = 16;
  }
  if (strlen($text) > 26) {
    $size = 15;
  }

  if (strlen($text) > 28) {
    $size = 14;
  }

  if (strlen($text) > 30) {
    $size = 13;
  }

  if (strlen($text) > 32) {
    $size = 12;
  }


  $font = __DIR__ . '/fonts/GothamBook.otf';
  imagettftext($img, $size, 0, 15, 33, $color, $font, $text);

  imagejpeg($img, 'addingtext/addingtext1.jpeg', 100);

}


function addNamePublic ($imgPath, $text) {

  $img = ImageCreateFromJPEG($imgPath);
  $color = imagecolorallocate($img, 191,213,222);

  $font = __DIR__ . '/fonts/gothambold.ttf';
  $size = 18;

  imagettftext($img, $size, 0, 15, 63, $color, $font, 'by ' . $text);
  imagejpeg($img, 'addingtext/addingtext2.jpeg', 100);

}

function addTracks ($imgPath, $text) {

  $img = ImageCreateFromJPEG($imgPath);
  $color = imagecolorallocate($img, 191,213,222);


  $font = __DIR__ . '/fonts/gothambold.ttf';
  imagettftext($img, 15, 0, 21, 120, $color, $font,  $text . ' tracks');

  imagejpeg($img, 'addingtext/addingtext3.jpeg', 100);
}

function addTime ($imgPath, $text) {

  $img = ImageCreateFromJPEG($imgPath);
  $color = imagecolorallocate($img, 191,213,222);


  $font = __DIR__ . '/fonts/gothambold.ttf';
  imagettftext($img, 15, 0, 21, 145, $color, $font,  $text . ' time');

  imagejpeg($img, 'addingtext/addingtext4.jpeg', 100);
}



function addDateMix ($imgPath, $text) {

  $img = ImageCreateFromJPEG($imgPath);
  $color = imagecolorallocate($img, 191,213,222);


  $font = __DIR__ . '/fonts/gothambold.ttf';
  imagettftext($img, 12, 0, 21, 234, $color, $font, $text);

  imagejpeg($img, 'final-image/image.jpeg', 100);

}