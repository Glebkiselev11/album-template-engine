function handleFileSelect(evt) {
  const files = evt.target.files;

  // Loop through the FileList and render image files as thumbnails.
  for (let i = 0, f; f = files[i]; i++) {

    // Only process image files.
    if (!f.type.match('image.*')) {
      continue;
    }

    const reader = new FileReader();
    

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        // Render thumbnail.
        const imageWrap = document.querySelector('.form__image-window');
        imageWrap.style.backgroundImage = `url('${e.target.result}')`;
      };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
  }
}

document.getElementById('inputfile').addEventListener('change', handleFileSelect, false);