
<?php 
$title = 'Template Engine Releases';
require 'includes/header.php';?>
<main>
  <form enctype="multipart/form-data" action="handler.php" method="POST" class="form-wrap">
    <div class="form__left-input-wrap">
      <label for="album_name" class="label_simple">Name of album</label>
      <input type="text" id="album_name" name="album_name" required>

      <label for="artist_name" class="label_simple">Name of artist</label>
      <input type="text" id="artist_name" name="artist_name" required>

      
      <label for="album_tracks" class="label_simple">How many tracks</label>
      <input type="number" id="album_tracks" name="album_tracks" required>

      <label for="date_rel" class="label_simple">Date of release</label>
      <input type="text" id="date_rel" name="date_rel" required>

      <label for="type" class="label_simple">Type of release</label>
      
      <div class="radio-wrap">
        <label for="ep" class="label_radio">
          <input type="radio" name="type" value="ep" id="ep" checked>
          Ep
        </label>
        <label for="album" class="label_radio">
          <input type="radio" name="type" value="album" id="album">
          Album
        </label>
      </div>
      
        
    
    </div>

    <div class="form__image-window-wrap">
      <label class="label_simple">Cover</label>
      <div class="form__image-window" id="image-form"></div>
      <label for="inputfile" class='btn-inputfile'>select a file</label>
      <input name="cover" type="file" id="inputfile" class="inputfile" required
>
    </div>
    

    <button type="submit" class='btn-submit'>Сreate template</button>

  </form>
</main>
<script src="scripts/main.js"></script>
</body>
</html>