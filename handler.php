<?php

//получение пост запроса с файла релизов
$rel_name = $_POST['album_name'];
$artist_name = $_POST['artist_name'];
$type = $_POST['type'];
$album_tracks = $_POST['album_tracks'];
$date_rel = $_POST['date_rel'];

//получение пост запроса с файла миксов
$mix_name = $_POST['mix_name'];
$public_name = $_POST['public'];
$mix_tracks = $_POST['mix_tracks'];
$time_mix = $_POST['time_mix'];


$date_of_mix = $_POST['date_of_mix'];




//Обработка загруженной обложки
$cover = $_FILES['cover']['name'];
$path = 'crop/';

if (!@copy($_FILES['cover']['tmp_name'], $path . $_FILES['cover']['name']))
  echo 'Что-то пошло не так';

//Обрезка обложки под нужный 192 пиксель
require 'cropimage.php';
if ($rel_name) {
  //Если передается из формы релизом, то обрезаем под нее
  cropImage("crop/". $cover, "cover/cover.JPEG", 192, 192);
} else {
  //Иначе это микс форма и обрезаем под микс
  cropImage("crop/". $cover, "cover/cover.JPEG", 242, 242);
}


//Добавление картинки в шаблон
require 'addcover.php';
if ($rel_name) {
  //Если передается из формы релизов, то берем соответствующий шаблон под фон
  addCover(__DIR__ . '\template', __DIR__ . '\cover', 'template_rel.jpeg', 'cover.JPEG', 304, 4);
} else {
  addCover(__DIR__ . '\template', __DIR__ . '\cover', 'template_mix.jpeg', 'cover.JPEG', 253, 4);
}


require 'addtext.php';


if($rel_name) {
  //Если передаются данные из релизов, то работаем с ними
  //Добавление названия альбома в шаблон
  addNameRel('addingtext/addingtext.jpeg', $rel_name);

  //Добавление названия артиста в шаблон
  addNameArtist('addingtext/addingtext1.jpeg', $artist_name);
  
  //Добавление типа
  addType('addingtext/addingtext2.jpeg', $album_tracks, $type);
  
  //Добавление даты
  addDate('addingtext/addingtext3.jpeg', $date_rel);
} else {
  //Иначе работаем с миксом
  //Добавление названия альбом в шаблон

  addNameMix('addingtext/addingtext.jpeg', $mix_name);

  //Добавление названия артиста в шаблон
  addNamePublic('addingtext/addingtext1.jpeg', $public_name);

  //Добавление типа
  addTracks('addingtext/addingtext2.jpeg', $mix_tracks);

  //Добавление длительности микса
  addTime('addingtext/addingtext3.jpeg', $time_mix);

  //Добавление даты
  addDateMix('addingtext/addingtext4.jpeg', $date_of_mix);
}


//Очистка директории кроп от накопившихся файлов

delCropDir();
$title = 'Result';
require 'includes/header.php';
?>
<div class="image-wrap">
  <label class="label_simple">Result:</label> 
  <img src='final-image/image.jpeg'>
  <a class="link-btn" href='index.php'>Return to the home page</a>
</div>

</body>
</html>



