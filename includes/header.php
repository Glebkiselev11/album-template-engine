<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php echo $title; ?></title>
  <link rel="stylesheet" href="styles/main.css">
  <link rel="icon" href="favicon/favicon.ico" type="image/x-icon">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
</head>
<body>
<nav class="nav">
  <div class="nav-wrap">
    <div class="nav__logo-wrap">
      <div class="nav__logo logo"></div>
      <span class="nav__name">Template Engine</span>
    </div>
    
    <ul class="nav__list">
      <li class="nav__item"><a href="/mixtemplate.php" class="nav__link
      <?php 
      if($title == 'Template Engine Mixes') {
        echo 'nav__link_active';
      }; 
      ?>">Mixes</a></li>
      <li class="nav__item"><a href="/" class="nav__link 
      <?php 
      if($title == 'Template Engine Releases') {
        echo 'nav__link_active';
      }; 
      ?>">Releases</a></li>
    </ul>
  </div>
</nav>