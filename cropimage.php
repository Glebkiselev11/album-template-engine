
<style>
  .wrap {
  display: flex;
  flex-wrap: wrap;
    justify-content: flex-start;
    width: 1500px;
    height: 6000px;
    margin: auto;
  }


</style>

<?php
/**
 * @param string $aInitialImageFilePath - строка, представляющая путь к обрезаемому изображению
 * @param string $aNewImageFilePath - строка, представляющая путь куда нахо сохранить выходное обрезанное изображение
 * @param int $aNewImageWidth - ширина выходного обрезанного изображения
 * @param int $aNewImageHeight - высота выходного обрезанного изображения
 */
function cropImage($aInitialImageFilePath, $aNewImageFilePath, $aNewImageWidth, $aNewImageHeight) {
  if (($aNewImageWidth < 0) || ($aNewImageHeight < 0)) {
    return false;
  }

  // Массив с поддерживаемыми типами изображений
  $lAllowedExtensions = array(1 => "gif", 2 => "jpeg", 3 => "png");

  // Получаем размеры и тип изображения в виде числа
  list($lInitialImageWidth, $lInitialImageHeight, $lImageExtensionId) = getimagesize($aInitialImageFilePath);

  if (!array_key_exists($lImageExtensionId, $lAllowedExtensions)) {
    return false;
  }
  $lImageExtension = $lAllowedExtensions[$lImageExtensionId];

  // Получаем название функции, соответствующую типу, для создания изображения
  $func = 'imagecreatefrom' . $lImageExtension;
  // Создаём дескриптор исходного изображения
  $lInitialImageDescriptor = $func($aInitialImageFilePath);

  // Определяем отображаемую область
  $lCroppedImageWidth = 500;
  $lCroppedImageHeight = 500;
  $lInitialImageCroppingX = 0;
  $lInitialImageCroppingY = 0;
  if ($aNewImageWidth / $aNewImageHeight > $lInitialImageWidth / $lInitialImageHeight) {
    $lCroppedImageWidth = floor($lInitialImageWidth);
    $lCroppedImageHeight = floor($lInitialImageWidth * $aNewImageHeight / $aNewImageWidth);
    $lInitialImageCroppingY = floor(($lInitialImageHeight - $lCroppedImageHeight) / 2);
  } else {
    $lCroppedImageWidth = floor($lInitialImageHeight * $aNewImageWidth / $aNewImageHeight);
    $lCroppedImageHeight = floor($lInitialImageHeight);
    $lInitialImageCroppingX = floor(($lInitialImageWidth - $lCroppedImageWidth) / 2);
  }

  // Создаём дескриптор для выходного изображения
  $lNewImageDescriptor = imagecreatetruecolor($aNewImageWidth, $aNewImageHeight);
  imagecopyresampled($lNewImageDescriptor, $lInitialImageDescriptor, 0, 0, $lInitialImageCroppingX, $lInitialImageCroppingY, $aNewImageWidth, $aNewImageHeight, $lCroppedImageWidth, $lCroppedImageHeight);
  $func = 'image' . $lImageExtension;

  // сохраняем полученное изображение в указанный файл
  return $func($lNewImageDescriptor, $aNewImageFilePath);
}



//очистка директории от хлама
function delCropDir () {
  $files = glob('crop/*'); // get all file names
  foreach($files as $file){ // iterate files
  if(is_file($file))
    unlink($file); // delete file
}
}





